package main

/**
Gorm 框架测试
*/
import (
	"database/sql"
	"fmt"
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
	"time"
)

type User struct {
	ID           uint
	Name         string
	Email        *string
	Age          uint8
	Birthday     time.Time
	MemberNumber sql.NullString
	ActivedAt    sql.NullTime
	CreatedAt    time.Time
	UpdatedAt    time.Time
}

func main() {
	// 参考 https://github.com/go-sql-driver/mysql#dsn-data-source-name 获取详情
	dsn := "kepler:123456@tcp(192.168.93.149:3306)/test?charset=utf8mb4&parseTime=True&loc=Local"
	db, err := gorm.Open(mysql.Open(dsn), &gorm.Config{})
	if(err != nil){
		return
	}
	//user := User{Name: "kepler", Age: 18, Birthday: time.Now()}
	//user_args := []User{{Name: "jinzhu1", Birthday: time.Now()},
	//	{Name: "jinzhu2", Birthday: time.Now()}, {Name: "jinzhu3", Birthday: time.Now()}}
	//
	//
	//db.AutoMigrate(&user)
	//result := db.Create(&user)
	//db.Create(user_args)
	//
	//fmt.Println(result.RowsAffected)
	//db.Commit()

	//result1 := db.Find(&user)
	//fmt.Println(result1.RowsAffected)
	//rows, err := result1.Rows()
	//defer rows.Close()
	//
	//for rows.Next(){
	//	var u1 User
	//	db.ScanRows(rows, &u1)
	//	fmt.Println(u1)
	//}

	// UPDATE users SET email='123', updated_at='2013-11-17 21:34:10' WHERE name=kepler;
	db.Model(&User{}).Where("name = ?", "kepler").Update("email", "123")

	rows1, err := db.Raw("select * from test.users where name != ?", "kepler").Rows()
	defer rows1.Close()

	for rows1.Next(){
		var u1 User
		db.ScanRows(rows1, &u1)
		fmt.Println(u1)
	}
}

