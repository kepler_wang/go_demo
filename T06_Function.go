package main

import "fmt"

//指向函数的指针
//对应Functional Interface
func andbodySay(f func(word string))  {
	f("hello")
}

func main(){
	var ff func(word string)

	var f1 = func(word string){
		fmt.Println(word, " f1")
	}

	ff = f1

	andbodySay(f1)
	andbodySay(ff)

	var f2 = func(word string){
		fmt.Println(word, " f2")
	}

	andbodySay(f2)

	f3 := f2
	andbodySay(f3)
}