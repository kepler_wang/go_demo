package main

import "fmt"

type P struct {
	name string
}

func m2(p P)  {  //go在传递对象参数时，会整体复制一个对象到方法参数内，所以原对象不会修改属性，修改的是复制的对象
	p.name = "lisi"
}

func m3(p *P)  { //使用指针修改原对象
	p.name = "lisi"
}

func fu(v *int){
	*v = 8  //解指针
}

func main() {
	var pp P = P{"zhangsan"}
	m2(pp)
	fmt.Println(pp)

	m3(&pp)
	fmt.Println(pp)

	m := 0
	fu(&m)
	fmt.Println(m)
}

