package main

import (
	"fmt"
	"io"
	"log"
	"net"
	"time"
)

func main()  {
	listener, err := net.Listen("tcp", "localhost:8888")
	if(err != nil){
		log.Fatal(err)
	}

	for{
		conn, err := listener.Accept()
		if(err != nil){
			log.Fatal(err)
		}
		fmt.Println("connection from client")
		//go类似于java的线程 但是不是，是GPM模型。类似于java线程池的任务
		go handle(conn) //异步执行
	}
}

func handle(conn net.Conn)  {
	defer conn.Close()
	for{
		_, err := io.WriteString(conn, "hello")
		if(err != nil){
			log.Fatal(err)
		}
		time.Sleep(1 * time.Second)
	}
}