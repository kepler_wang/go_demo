## go语言介绍

（1）指针

指针描述符：*  取地址符：&
C语言的指针繁琐在指针运算 go内没有指针运算

（2）go并发

抢java c c++ php市场的关键点
Java线程

Go routine 协程

协程不比线程池快，有些情况下比线程快，一万个协程比一万个线程快

线程池的task，不具备协调能力； 
golang中可以用channel（java SynchronizedQueue）做协程的协调，相当于在用户空间模拟了CPU切换

（3）GPM模型
Goroutine Processor Machine
实际测试的情况下 Netty比go快
协程执行完回收

![images](images/Goroutine.png)

（4）golang中的异步：没有异步，因为到处都是异步，同步的写法，异步的效果

异步返回：只要goroutine向channl内扔数据即可

（5）gc
单纯比较gc，golang不如jdk9，go的gc使用的是三色标记

（6）go使用比较多的地方
内部基层的服务框架，基础类框架，消息网络中间件

## go的内存管理
go的gc与JVM不同，与操作系统的虚拟内存与物理内存的对应关系有关

![images](images/go内存管理1.png)

JVM内存管理一定存在移动对象的过程，比对象不移动偏低

go 内存管理核心：

（1）虚拟内存与物理内存的对应关系
（2）对象回收时不移动
（3）对对象进行分类一共67种，每种分配不同的标准页
（4）>32k的对象单独处理，单独分配空间
