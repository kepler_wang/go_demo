package main

import "fmt"

func main() {
	myArr := []int{1,2,3,4,5}
	mySlice1 := myArr[2:4]
	fmt.Println(mySlice1)

	mySliceEmpty := make([]int,2,4)
	fmt.Println(mySliceEmpty)

	mySliceEmpty2 := make([]int,5)
	fmt.Println(mySliceEmpty2)
}